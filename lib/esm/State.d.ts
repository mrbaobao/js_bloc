import { Equatable } from "./Extension";
export declare abstract class BlocState extends Equatable {
    toString: () => string;
    get props(): any[] | null;
}
