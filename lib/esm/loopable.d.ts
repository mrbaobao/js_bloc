declare class Loopable {
    private readonly list;
    private readonly customizers;
    constructor(list: any[]);
    /**
     * Add customizer
     */
    add: (modifier: (item: any) => void) => void;
    /**
     * Exec
     */
    loop: (f: (item: any, index: number) => void) => any[];
}
/**
 * Make list loop
 * @param list
 * @returns
 */
declare const make: (list: any[]) => Loopable;
export { make, Loopable };
