import { Equatable } from "./Extension";
export declare abstract class BlocEvent extends Equatable {
    toString: () => string;
    get props(): any[] | null;
}
