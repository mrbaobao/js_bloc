"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Equatable = void 0;
/**
 * Equatable
 */
class Equatable {
    constructor() {
        this.toString = () => 'Equatable';
    }
    //Props
    get props() {
        return null;
    }
    /**
     * Equals to
     * @param e
     */
    equals(e) {
        return Equatable.compare(this, e);
    }
    /**
     * Compare two Equatable is equal or not
     * @param a
     * @param b
     */
    static compare(a, b) {
        //Compare class name
        if (a.toString() !== b.toString()) {
            //Not Equal:
            return false;
        }
        //Compare props
        if (a.props === null) {
            return b.props === null;
        }
        //a.props !== null
        if (b.props === null) {
            return false;
        }
        //a.props, b.props !== null
        if (a.props.length !== b.props.length) {
            return false;
        }
        //a.props.length === b.props.length
        for (let i = 0; i < a.props.length; i++) {
            if (a.props[i] !== b.props[i]) {
                return false;
            }
        }
        //OK
        return true;
    }
}
exports.Equatable = Equatable;
