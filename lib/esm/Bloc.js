"use strict";
var __asyncValues = (this && this.__asyncValues) || function (o) {
    if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
    var m = o[Symbol.asyncIterator], i;
    return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () { return this; }, i);
    function verb(n) { i[n] = o[n] && function (v) { return new Promise(function (resolve, reject) { v = o[n](v), settle(resolve, reject, v.done, v.value); }); }; }
    function settle(resolve, reject, d, v) { Promise.resolve(v).then(function(v) { resolve({ value: v, done: d }); }, reject); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Bloc = exports.Transition = exports.Change = void 0;
const rxjs_1 = require("rxjs");
/**
 * class: Change
 */
class Change {
    constructor(currentState, nextState) {
        this.currentState = currentState;
        this.nextState = nextState;
    }
}
exports.Change = Change;
/**
 * class: Transition
 */
class Transition {
    constructor(currentState, event, nextState) {
        this.currentState = currentState;
        this.nextState = nextState;
        this.event = event;
    }
}
exports.Transition = Transition;
/**
 * main class: Bloc
 */
class Bloc {
    constructor(initialState) {
        //keep track of the events coming in
        this._events = new rxjs_1.BehaviorSubject(null);
        //we need an initial state for our behaviour subjects
        this.state = this.InitialState = initialState;
        //init our subject
        this._states = new rxjs_1.BehaviorSubject(initialState);
        //init our external observable that stuff listens to
        // this.state = this._states;
        //every time we get a new event run the child method
        this._events.subscribe((event) => {
            if (!event) {
                return;
            }
            this.dispatchEvent(event);
        });
    }
    //async read all the yields coming from the child method
    async dispatchEvent(event) {
        var e_1, _a;
        var stateIterator = this.mapEventToState(event);
        try {
            //fancy shizzle to keep reading the asyncgenerator
            for (var stateIterator_1 = __asyncValues(stateIterator), stateIterator_1_1; stateIterator_1_1 = await stateIterator_1.next(), !stateIterator_1_1.done;) {
                let state = stateIterator_1_1.value;
                if (!this.state.equals(state)) {
                    this.onTransition(new Transition(this.state, event, state));
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (stateIterator_1_1 && !stateIterator_1_1.done && (_a = stateIterator_1.return)) await _a.call(stateIterator_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
    }
    //receive new events
    add(event) {
        //add the event
        this._events.next(event);
    }
    //stop observations
    close() {
        this._events.complete();
        this._states.complete();
    }
    //Listen to state changes
    listen(listener) {
        return this._states.subscribe(listener);
    }
    //Call when state changed, after onTransition
    //@needs-call-super
    onChange(change) {
        this.state = change.nextState;
        this._states.next(change.nextState);
    }
    //Call when state changed
    //@needs-call-super
    onTransition(transition) {
        this.onChange(new Change(transition.currentState, transition.nextState));
    }
}
exports.Bloc = Bloc;
