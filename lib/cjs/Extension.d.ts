/**
 * Stream
 */
export declare type Stream<T = unknown, TRturn = any, TNext = unknown> = AsyncGenerator<T, TRturn, TNext>;
/**
 * Equatable
 */
export declare abstract class Equatable {
    toString: () => string;
    get props(): any[] | null;
    /**
     * Equals to
     * @param e
     */
    equals(e: Equatable): boolean;
    /**
     * Compare two Equatable is equal or not
     * @param a
     * @param b
     */
    static compare(a: Equatable, b: Equatable): boolean;
}
