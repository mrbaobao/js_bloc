"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BlocEvent = void 0;
const Extension_1 = require("./Extension");
class BlocEvent extends Extension_1.Equatable {
    constructor() {
        super(...arguments);
        this.toString = () => 'BlocEvent';
    }
    //Props
    get props() {
        return null;
    }
}
exports.BlocEvent = BlocEvent;
