import { Subscription } from "rxjs";
import { BlocEvent } from "./Event";
import { BlocState } from "./State";
/**
 * class: Change
 */
export declare class Change<StateType extends BlocState> {
    currentState: StateType;
    nextState: StateType;
    constructor(currentState: StateType, nextState: StateType);
}
/**
 * class: Transition
 */
export declare class Transition<EventType extends BlocEvent, StateType extends BlocState> {
    currentState: StateType;
    nextState: StateType;
    event: EventType;
    constructor(currentState: StateType, event: EventType, nextState: StateType);
}
/**
 * main class: Bloc
 */
export declare abstract class Bloc<EventType extends BlocEvent, StateType extends BlocState> {
    private _events;
    private _states;
    state: StateType;
    InitialState: StateType;
    constructor(initialState: StateType);
    private dispatchEvent;
    add(event: EventType): void;
    abstract mapEventToState(event: EventType): AsyncGenerator<StateType>;
    close(): void;
    listen(listener: (state: StateType) => void): Subscription;
    protected onChange(change: Change<StateType>): void;
    protected onTransition(transition: Transition<EventType, StateType>): void;
}
