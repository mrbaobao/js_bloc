"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Loopable = exports.make = void 0;
class Loopable {
    //Construct with a list
    constructor(list) {
        this.list = list;
        //Customizers
        this.customizers = [];
        /**
         * Add customizer
         */
        this.add = (modifier) => {
            this.customizers.push(modifier);
        };
        /**
         * Exec
         */
        this.loop = (f) => {
            //Last customizer
            if (f) {
                this.customizers.push(f);
            }
            //Loop
            const count = this.list.length;
            const cCount = this.customizers.length;
            for (let i = 0; i < count; i++) {
                for (let j = 0; j < cCount; j++) {
                    this.customizers[j](this.list[i], i);
                }
            }
            //Return list
            return this.list;
        };
    }
}
exports.Loopable = Loopable;
/**
 * Make list loop
 * @param list
 * @returns
 */
const make = (list) => {
    return new Loopable(list);
};
exports.make = make;
