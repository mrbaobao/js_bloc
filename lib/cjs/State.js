"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BlocState = void 0;
const Extension_1 = require("./Extension");
class BlocState extends Extension_1.Equatable {
    constructor() {
        super(...arguments);
        this.toString = () => 'BlocState';
    }
    //Props
    get props() {
        return null;
    }
}
exports.BlocState = BlocState;
