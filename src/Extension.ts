/**
 * Stream
 */
export type Stream<T = unknown, TRturn = any, TNext = unknown> = AsyncGenerator<T, TRturn, TNext>
/**
 * Equatable
 */
export abstract class Equatable {
    toString = () => 'Equatable'
    //Props
    get props(): any[] | null {
        return null
    }
    /**
     * Equals to
     * @param e 
     */
    equals(e: Equatable): boolean {
        return Equatable.compare(this, e)
    }
    /**
     * Compare two Equatable is equal or not
     * @param a 
     * @param b 
     */
    static compare(a: Equatable, b: Equatable): boolean {
        //Compare class name
        if (a.toString() !== b.toString()) {
            //Not Equal:
            return false
        }
        //Compare props
        if (a.props === null) {
            return b.props === null
        }
        //a.props !== null
        if (b.props === null) {
            return false
        }
        //a.props, b.props !== null
        if (a.props.length !== b.props.length) {
            return false
        }
        //a.props.length === b.props.length
        for (let i = 0; i < a.props.length; i++) {
            if (a.props[i] !== b.props[i]) {
                return false
            }
        }
        //OK
        return true
    }
}