# js_bloc

BLoC pattern for Javascript/Typescript.
If you need BLoC pattern for Reactjs project, please use reactjs_bloc: [reactjs_bloc](https://www.npmjs.com/package/reactjs_bloc)

# Install:
* yarn add js_bloc
* npm i js_bloc

# Usage:
1. BlocState
```
//MyState.ts
import {BlocState} from "js_bloc"
//Base State
export class MyState extends BlocState {
    //Must have toString method!!!
    //Using for compare two different classes
    //Reason: After minify Javascript source code, two classes may be have same name!!!
    toString = () => "MyState"
}
export class InitialState extends MyState {
    //Must have toString method!!!
    //Using for compare two different classes
    //Reason: After minify Javascript source code, two classes may be have same name!!!
    toString = () => "InitialMyState"
}
export class RunningState extends MyState {
    //Must have toString method!!!
    //Using for compare two different classes
    //Reason: After minify Javascript source code, two classes may be have same name!!!
    toString = () => "RunningState"
    speed: number
    get props() {
        return [this.speed]
    }
}
```

2. BlocEvent: similar to BlocState
```
//MyEvent.ts
```

3. Bloc
```
//MyBloc.ts
import { Stream, Bloc } from "js_bloc";
import {MyState, AuthState2, LoadingMyState} from "./MyState"
import {MyEvent, AppStartedMyEvent, MyEvent2} from "./MyEvent"
class MyBloc extends Bloc<MyEvent, MyState> {
    /**
     * Constructor
     */
    constructor() {
        super(new InitialState())
    }
    /**
     * Map event to state
     */
    async *mapEventToState(event: MyEvent): Stream<MyState> {
        //Event --> yield new state
        if (event instanceof AppStartedMyEvent) {
            //Do somthing here...
            ...
            //New sate
            yield new LoadingMyState()
        }
        //Event 2
        if (event instanceof MyEvent2) {
            //Do somthing here...
            ...
            //New sate
            yield new AuthState2()
        }
    }
}
//Export
export default MyBloc
```

4. Usage:
```
    /**
     * My bloc
     */
    const myBloc = new MyBloc();
    //Listen change
    myBloc.listen((state: MyState) => {
        console.log("My state change: ", state)
    })
    //Add new event
    myBloc.add(new AppStartedMyEvent)
    //Close
    myBloc.close()
```

